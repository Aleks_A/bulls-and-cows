from algo import validate, verify_unique, verify_attempts, long_rules, short_rules

import time
import random

import telepot
from telepot.delegate import per_chat_id, create_open, pave_event_space
from telepot.loop import MessageLoop


class BotMaster:
    """ Подключается к серверу и создает отдельного бота для каждого чата """

    def __init__(self):
        self.token = self.read_key()

        self.bot = telepot.DelegatorBot(self.token,
                                        [pave_event_space()(per_chat_id(), create_open, ChatBot, timeout=300)])

    def loop(self):
        print("Как всё запущено...")

        MessageLoop(self.bot).run_as_thread()

        while 1:
            time.sleep(60)

    @staticmethod
    def read_key():
        key_file = "key.txt"
        with open(key_file, 'r') as f:
            return f.readline()


class ChatBot(telepot.helper.ChatHandler):
    """ Отдельный объект на каждый чат """

    # noinspection SpellCheckingInspection
    def __init__(self, *args, **kwargs):
        super(ChatBot, self).__init__(*args, **kwargs)

        self.answer = 0
        self.attempts = 0
        self.max_attempts = 16
        self.stickers = {"win": "CAADAgADEwAD-6FeAv5vpqVWzNx5Ag",
                         "lose": "CAADAgADFAAD-6FeAgpafPLqbwEmAg"}

    def reset_answer(self):
        self.answer = 0

    def game_in_progress(self):
        return self.answer != 0

    def on_chat_message(self, msg):
        """ Направляет сообщения пользователя в соответствующий обработчик """

        content_type, chat_type, chat_id = telepot.glance(msg)
        if content_type == "text":
            # print(msg["from"]["id"], msg["text"])

            if msg["text"].startswith("/play"):
                self.on_play(msg["text"])
            elif msg["text"] == "/hint":
                self.on_hint()
            elif msg["text"] == "/help":
                self.on_help()
            else:
                """ assume the number is given """
                self.check_answer(msg)
        else:
            print(content_type, msg)

    def reply(self, text, message_id):
        self.bot.sendMessage(self.chat_id, text, reply_to_message_id=message_id)

    def send_sticker(self, code):
        if code in self.stickers.keys():
            self.bot.sendSticker(self.chat_id, self.stickers[code])

    def answer_is_valid(self, answer, message_id):
        if len(answer) != 4:
            self.reply("Введите четырёхзначное число", message_id)
            return False

        if answer[0] == "0":
            self.reply("Число не может начинаться с нуля", message_id)
            return False

        try:
            in_int = int(answer)
        except ValueError:
            self.reply("Введите четырёхзначное число", message_id)
            return False

        return in_int

    def check_answer(self, msg):
        message_id = msg["message_id"]
        message_text = msg["text"]

        """ Игра не начата """
        if not self.game_in_progress():
            self.reply(short_rules(), message_id)
            return

        guess = self.answer_is_valid(message_text, message_id)

        """ Ввод невалиден """
        if not guess:
            return

        self.attempts += 1

        """ Проверяем попытку """
        response = validate(guess, self.answer)

        """ Ты угадал """
        if response["result"]:
            self.reply("Ты угадал. Число попыток - {}".format(self.attempts), message_id)
            self.send_sticker("win")
            self.reset_answer()
        else:
            if verify_attempts(self.attempts, self.max_attempts):
                """ попытки еще есть, пробуй еще раз """
                self.reply(response["text"], message_id)
            else:
                """ попыток больше нет, ты олень """
                self.bot.sendMessage(self.chat_id, "Попытки закончились.\nТы олень ;-)")
                self.bot.send_sticker("lose")
                self.reset_answer()

    def on_play(self, text):
        """ Начинаем игру по команде play """

        """ Загадываем число """
        while not verify_unique(self.answer):
            """ Случайное число с разными цифрами и не начинающееся с нуля """
            self.answer = random.randint(1023, 9876)
            print("Я попробовал загадать число {}".format(self.answer))

        """ Устанавливаем число попыток """
        s = text.split(" ")

        if len(s) == 2:
            """ У нас есть параметр с числом попыток """
            try:
                self.max_attempts = int(s[1])
            except ValueError:
                self.bot.sendMessage(self.chat_id, "Введите число попыток, например /play 12")
                return

        self.attempts = 0

        self.bot.sendMessage(self.chat_id, "Угадай число. Попыток - {}.".format(self.max_attempts))

    def on_hint(self):
        if self.game_in_progress():
            self.bot.sendMessage(self.chat_id, "Я загадал {0}. Число твоих попыток: {1} из {2}."
                                 .format(self.answer, self.attempts, self.max_attempts))
        else:
            self.bot.sendMessage(self.chat_id, short_rules())

    def on_help(self):
        for key in long_rules().keys():
            self.bot.sendMessage(self.chat_id, long_rules()[key])

    def on_start(self):
        pass

    def on__idle(self, event):
        self.close()

    def close(self):
        pass
        # print("Closing", self.chat_id)


"""
Actual launch
"""

bot = BotMaster()
bot.loop()
