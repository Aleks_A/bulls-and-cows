import unittest


# noinspection PyPep8
def long_rules():
    return {"text1": "После команды /play Бот загадывает 4-значное число с неповторяющимися цифрами.",
            "text2": "После команды /play через пробел можно ввести количество попыток, за которое Вы собираетесь угадать загаданное число (по умолчанию 16).",
            "text3": "После каждой попытки Бот сообщает в ответ, сколько цифр угадано в числе (т.е. количество коров) и сколько угадано позиций цифр в числе (т.е. количество быков).",
            "text4": "Например:Бот загадал число «3219». Ваша попытка: «2310» . Бот отправит в ответ: один бык (цифра «1» угадана вплоть до позиции) и две коровы (цифры: «2» и «3» — угаданы, но на неверных позициях).",
            "text5": "Победой считаются 4 быка за нужное количество попыток. В противном случае Вы становитесь оленем.",
            "text6": "Хорошей игры) ."}


def short_rules():
    return "Введите команду /play чтобы начать игру или /help чтобы узнать правила."


def verify_attempts(count, maximum):
    if count >= maximum:
        return False
    else:
        return True


def verify_unique(r):
    n1 = r // 1000
    n2 = (r // 100) % 10
    n3 = (r % 100) // 10
    n4 = (r % 10)

    # print(n1, n2, n3, n4)

    if (n1 == n2) or (n1 == n3) or (n1 == n4) or (n2 == n3) or (n2 == n4) or (n3 == n4):
        return False
    else:
        return True


def validate(guess, answer):
    guess_string = str(guess)
    answer_string = str(answer)
    bulls = 0
    cows = 0

    if guess == answer:
        return {"result": True, "text": "Молодец! :)"}

    if len(guess_string) != 4:
        return {"result": False, "text": "Введите четырёхзначное число"}

    if not verify_unique(guess):
        return {"result": False, "text": "Все цифры должны быть разными"}

    """ считаем коров """
    for digit in guess_string:
        if digit in answer_string:
            cows = cows + 1

    """ считаем быков """
    for i in range(0, 4):
        if guess_string[i] == answer_string[i]:
            bulls = bulls + 1

    cows = cows - bulls

    return {"result": False, "text": "Быков {}. Коров {}. Пробуй ещё".format(bulls, cows)}


class ValidateAlgo(unittest.TestCase):
    """ Проверяем логику работы бота """

    def test_answers(self):
        """ Проверяем введенные числа """
        self.assertTrue(validate(1234, 1234)["result"])
        self.assertFalse(validate(123, 1234)["result"])
        self.assertFalse(validate(1231, 1234)["result"])
        self.assertFalse(validate(1235, 1234)["result"])

    def test_attempts(self):
        """ Проверяем подсчет попыток """
        self.assertTrue(verify_attempts(2, 18))
        self.assertFalse(verify_attempts(3, 3))
        self.assertFalse(verify_attempts(4, 3))


""" Код ниже выполняется, если этот файл запускается отдельно """
if __name__ == "__main__":
    unittest.main(verbosity=2)
